/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author aelgueta
 */
public class Promocion {
    private int idPromocion;
    private int idProducto;
    private int descuento;

    public Promocion() {
    }

    public Promocion(int idPromocion, int idProducto, int descuento) {
        this.idPromocion = idPromocion;
        this.idProducto = idProducto;
        this.descuento = descuento;
    }

    public int getIdPromocion() {
        return idPromocion;
    }

    public void setIdPromocion(int idPromocion) {
        this.idPromocion = idPromocion;
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int idProducto) {
        this.idProducto = idProducto;
    }

    public int getDescuento() {
        return descuento;
    }

    public void setDescuento(int descuento) {
        this.descuento = descuento;
    }

    @Override
    public String toString() {
        return "Promocion{" + "idPromocion=" + idPromocion + ", idProducto=" + idProducto + ", descuento=" + descuento + '}';
    }
    
    
    
}
