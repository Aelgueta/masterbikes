/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author aelgueta
 */
public class Empresa {
    private int idEmpresa;
    private String nombreEmpresa;
    private String rutConvenio;
    private int descuento;

    public Empresa() {
    }

    public Empresa(int idEmpresa, String nombreEmpresa, String rutConvenio, int descuento) {
        this.idEmpresa = idEmpresa;
        this.nombreEmpresa = nombreEmpresa;
        this.rutConvenio = rutConvenio;
        this.descuento = descuento;
    }

    public int getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    public String getRutConvenio() {
        return rutConvenio;
    }

    public void setRutConvenio(String rutConvenio) {
        this.rutConvenio = rutConvenio;
    }

    public int getDescuento() {
        return descuento;
    }

    public void setDescuento(int descuento) {
        this.descuento = descuento;
    }

    @Override
    public String toString() {
        return "Empresa{" + "idEmpresa=" + idEmpresa + ", nombreEmpresa=" + nombreEmpresa + ", rutConvenio=" + rutConvenio + ", descuento=" + descuento + '}';
    }
    
    
    
    
}
