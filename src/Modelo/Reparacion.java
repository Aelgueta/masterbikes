/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author aelgueta
 */
public class Reparacion {
    private int idReparacion;
    private String rutCliente;
    private int idEstadoReparacion;
    private int montoTotal;
    private String fechaEntregaEstimada;
    private String rutTrabajador;

    public Reparacion() {
    }

    public Reparacion(int idReparacion, String rutCliente, int idEstadoReparacion, int montoTotal, String fechaEntregaEstimada, String rutTrabajador) {
        this.idReparacion = idReparacion;
        this.rutCliente = rutCliente;
        this.idEstadoReparacion = idEstadoReparacion;
        this.montoTotal = montoTotal;
        this.fechaEntregaEstimada = fechaEntregaEstimada;
        this.rutTrabajador = rutTrabajador;
    }

    public int getIdReparacion() {
        return idReparacion;
    }

    public void setIdReparacion(int idReparacion) {
        this.idReparacion = idReparacion;
    }

    public String getRutCliente() {
        return rutCliente;
    }

    public void setRutCliente(String rutCliente) {
        this.rutCliente = rutCliente;
    }

    public int getIdEstadoReparacion() {
        return idEstadoReparacion;
    }

    public void setIdEstadoReparacion(int idEstadoReparacion) {
        this.idEstadoReparacion = idEstadoReparacion;
    }

    public int getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(int montoTotal) {
        this.montoTotal = montoTotal;
    }

    public String getFechaEntregaEstimada() {
        return fechaEntregaEstimada;
    }

    public void setFechaEntregaEstimada(String fechaEntregaEstimada) {
        this.fechaEntregaEstimada = fechaEntregaEstimada;
    }

    public String getRutTrabajador() {
        return rutTrabajador;
    }

    public void setRutTrabajador(String rutTrabajador) {
        this.rutTrabajador = rutTrabajador;
    }

    @Override
    public String toString() {
        return "Reparacion{" + "idReparacion=" + idReparacion + ", rutCliente=" + rutCliente + ", idEstadoReparacion=" + idEstadoReparacion + ", montoTotal=" + montoTotal + ", fechaEntregaEstimada=" + fechaEntregaEstimada + ", rutTrabajador=" + rutTrabajador + '}';
    }
    
    
    
    
}
