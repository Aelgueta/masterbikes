/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author aelgueta
 */
public class EstadoReparacion {
    private int idEstadoReparacion;
    private String descripcion;

    public EstadoReparacion() {
    }

    public EstadoReparacion(int idEstadoReparacion, String descripcion) {
        this.idEstadoReparacion = idEstadoReparacion;
        this.descripcion = descripcion;
    }

    public int getIdEstadoReparacion() {
        return idEstadoReparacion;
    }

    public void setIdEstadoReparacion(int idEstadoReparacion) {
        this.idEstadoReparacion = idEstadoReparacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Override
    public String toString() {
        return "EstadoReparacion{" + "idEstadoReparacion=" + idEstadoReparacion + ", descripcion=" + descripcion + '}';
    }
    
    
}
